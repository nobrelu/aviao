package br.com.itau.bootcamp.consumeropenbanking.port;

import br.com.itau.bootcamp.consumeropenbanking.comunication.http.controllers.dto.CountDTO;
import br.com.itau.bootcamp.consumeropenbanking.core.domain.Localidade;
import br.com.itau.bootcamp.consumeropenbanking.core.usecase.BuscarQuantidadeCorrespondentesBancariosUC;
import br.com.itau.bootcamp.consumeropenbanking.core.usecase.ContarStatus;
import br.com.itau.bootcamp.consumeropenbanking.port.converter.CountConverter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
@Slf4j
public class CoreUseCasePort {

    BuscarQuantidadeCorrespondentesBancariosUC buscarQuantidadeCorrespondentesBancariosUC;

    ContarStatus contarStatus;

    public CountDTO buscarQuantidadeCorrespondentesBancarios() {
        log.info("Inicio do port de controller Correspondente Bancário para o core de buscar quantidade");
        CountDTO countDTO = CountConverter.toDTO(buscarQuantidadeCorrespondentesBancariosUC.execute());
        log.info("Fim do port. Resultado de busca convertido: {}", countDTO);
        return countDTO;
    }

    public Integer buscaStatus() {
        return contarStatus.execute();
    }
}

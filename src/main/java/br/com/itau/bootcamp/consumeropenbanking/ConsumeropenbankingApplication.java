package br.com.itau.bootcamp.consumeropenbanking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ConsumeropenbankingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumeropenbankingApplication.class, args);
	}

}

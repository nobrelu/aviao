package br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.impl;

import br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.dto.ResponseCorrespondenteBancarioDTO;
import br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.dto.status.ResponseStatusDisponibilidadeDTO;
import br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.dto.status.StatusDTO;
import br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.feign.OpenbankingFeign;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Slf4j
@AllArgsConstructor
public class OpenBankingImpl {

    private OpenbankingFeign openbankingFeign;
    //porque 25?
    private static final int TOTAL_PAGES = 25;

    public ResponseCorrespondenteBancarioDTO buscarCorrespondentesBancarios(){
        log.info("Iniciando busca de Correspondentes Bancários");

        String saida = openbankingFeign.buscarCorrespondentesBancarios();

        ResponseCorrespondenteBancarioDTO responseCorrespondenteBancarioDTO;

        try {
            responseCorrespondenteBancarioDTO = new ObjectMapper().readValue(saida, ResponseCorrespondenteBancarioDTO.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        log.info("Resultado da busca de correspondentes Bancários: {} ", responseCorrespondenteBancarioDTO);

        return responseCorrespondenteBancarioDTO;

    }

    public List<StatusDTO> getStatus() {
        log.info("Iniciando busca do status do Openbanking");

        ResponseEntity<ResponseStatusDisponibilidadeDTO> response = openbankingFeign.getStatus(null, null);

        List<StatusDTO> statusDTOS = response.getBody().getDados().getStatusDTO();

        for(int i = 2; i <= response.getBody().getMetaDTO().getTotalPages(); i++) {
            ResponseEntity<ResponseStatusDisponibilidadeDTO> responsePaginado = openbankingFeign.getStatus(i, TOTAL_PAGES);
            statusDTOS.addAll(responsePaginado.getBody().getDados().getStatusDTO());
        }

        log.info("Resposta da requisição de status do Openbanking: {}", response.getBody());
        return statusDTOS;
    }

}

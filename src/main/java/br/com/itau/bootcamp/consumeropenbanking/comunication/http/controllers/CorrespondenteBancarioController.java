package br.com.itau.bootcamp.consumeropenbanking.comunication.http.controllers;

import br.com.itau.bootcamp.consumeropenbanking.comunication.http.controllers.dto.CountDTO;
import br.com.itau.bootcamp.consumeropenbanking.port.CoreUseCasePort;
import feign.Response;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//fim do endpoint aqui
@RestController
@AllArgsConstructor
@RequestMapping(value = "/correspondente-bancario")
@Slf4j
public class CorrespondenteBancarioController {

    private final CoreUseCasePort port;

    @GetMapping(value = "/count", produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<CountDTO> buscarQuantidadeCorrespondentesBancarios() {
        log.info("Inicio do controller de busca quantidade de correspondentes bancarios");

        CountDTO dto = port.buscarQuantidadeCorrespondentesBancarios();

        ResponseEntity<CountDTO> response = ResponseEntity.ok(dto);

        log.info("retorno da busca de quantidade de correspondentes bancarios: {}", response);

        return response;
    }

}

package br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class LinksDTO {
    @JsonProperty("self")
    private String self;

    @JsonProperty("first")
    private String first;

    @JsonProperty("prev")
    private String prev;

    @JsonProperty("next")
    private String next;

    @JsonProperty("last")
    private String last;




}

package br.com.itau.bootcamp.consumeropenbanking.core.domain;


import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Marca {
    private String nome;
    private List<Empresa> empresas;
}

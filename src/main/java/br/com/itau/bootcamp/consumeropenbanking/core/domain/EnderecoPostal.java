package br.com.itau.bootcamp.consumeropenbanking.core.domain;


import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EnderecoPostal {
    private String endereco;
    private String nomeBairro;
    private String nomeCidade;
    private String siglaEstado;
    private String codigoPostal;
    private String informacaoAdicional;

}

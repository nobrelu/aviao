package br.com.itau.bootcamp.consumeropenbanking.core.domain;



import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CorrespondenteBancario {
    private Identificacao identificacao;
    private List<Localidade> localidades;
    private List<Servico> servicos;

}

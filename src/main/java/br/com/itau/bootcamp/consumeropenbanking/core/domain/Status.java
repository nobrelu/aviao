package br.com.itau.bootcamp.consumeropenbanking.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.util.List;


@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Status {
    private String code;
    private String explanation;
    private String detectionTime;
    private String expectedResolutionTime;
    private String updateTime;
    private List<String> unavailableEndpoints;
}

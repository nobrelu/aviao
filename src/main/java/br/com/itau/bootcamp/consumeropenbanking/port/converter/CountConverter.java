package br.com.itau.bootcamp.consumeropenbanking.port.converter;

import br.com.itau.bootcamp.consumeropenbanking.comunication.http.controllers.dto.CountDTO;
import br.com.itau.bootcamp.consumeropenbanking.core.domain.Contratante;
import br.com.itau.bootcamp.consumeropenbanking.core.domain.CorrespondenteBancario;
import br.com.itau.bootcamp.consumeropenbanking.core.domain.Localidade;

import java.util.List;

public class CountConverter {
    public static CountDTO toDTO( List quantidade){
        return CountDTO.builder()
                .quantidadeCorrespondentesBancarios(quantidade)
                .build();
    }
}

package br.com.itau.bootcamp.consumeropenbanking.port.converter;



import br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.dto.ResponseCorrespondenteBancarioDTO;
import br.com.itau.bootcamp.consumeropenbanking.core.domain.*;

import java.util.stream.Collectors;

public class MarcaConverter {
    public static Marca toDomain(ResponseCorrespondenteBancarioDTO dto) {

        //construindo o objeto Marca, que internamente, contém os dados de Correspondete que precisamos
        //Este é um objeto bem complexo, mas se prestar atenção cada classe construída tem seu builder e as listas, usamos lambda para montar o objeto
        ResponseCorrespondenteBancarioDTO.Dados dados = dto.getDados();
        return Marca.builder()
                .nome(dados.getMarca().getNome())
                //lambda para montar a lista de empresas
                .empresas(dados.getMarca().getEmpresas().stream().map(
                        empresas -> Empresa.builder()
                                .nome(empresas.getNome())
                                .numeroCnpj(empresas.getNumeroCnpj())
                                //lambda para montar a lista de contratantes
                                .contratantes(empresas.getContratantes().stream().map(
                                        contratante -> Contratante.builder()
                                                .nome(contratante.getNome())
                                                .numeroCnpj(contratante.getNumeroCnpj())
                                                //lambda para montar a lista de correspondentes
                                                .correspondentesBancarios(contratante.getCorrespondentesBancarios().stream().map(
                                                        correspondente -> CorrespondenteBancario.builder()
                                                                .identificacao(Identificacao.builder()
                                                                        .nomeCorporativo(correspondente.getIdentificacao().getNomeCorporativo())
                                                                        .numeroCnpj(correspondente.getIdentificacao().getNumeroCnpj())
                                                                        .build())
                                                                //lambda para montar a lista de localidades
                                                                .localidades(correspondente.getLocalidades().stream().map(
                                                                        localidade -> Localidade.builder()
                                                                                .enderecoPostal(EnderecoPostal.builder()
                                                                                        .endereco(localidade.getEnderecoPostal().getEndereco())
                                                                                        .nomeBairro(localidade.getEnderecoPostal().getNomeBairro())
                                                                                        .nomeCidade(localidade.getEnderecoPostal().getNomeCidade())
                                                                                        .siglaEstado(localidade.getEnderecoPostal().getSiglaEstado())
                                                                                        .codigoPostal(localidade.getEnderecoPostal().getCodigoPostal())
                                                                                        .informacaoAdicional(localidade.getEnderecoPostal().getInformacaoAdicional())
                                                                                        .build())


                                                                                .build()).collect(Collectors.toList())) //fim localidades
                                                                .servicos(correspondente.getServicos().stream().map(
                                                                        servico -> Servico.builder()
                                                                                .nome(servico.getNome())
                                                                                .codigo(servico.getCodigo())

                                                                                .build()).collect(Collectors.toList())) //fim servicos

                                                                .build()
                                                ).collect(Collectors.toList())) //fim correspondentes
                                                .build()

                                ).collect(Collectors.toList())) //fim contratantes

                                .build()).collect(Collectors.toList()))
                .build();


    }
}

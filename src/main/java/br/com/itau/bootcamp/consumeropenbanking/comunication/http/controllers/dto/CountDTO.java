package br.com.itau.bootcamp.consumeropenbanking.comunication.http.controllers.dto;


import br.com.itau.bootcamp.consumeropenbanking.core.domain.Contratante;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder // o que faz o builder do lombok?
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL) // que isso, gente?
@JsonTypeName(value = "data") // e  isso?
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class CountDTO {
    @JsonProperty("quantidade-correspondentes-bancarios")
    private List<Contratante> quantidadeCorrespondentesBancarios;
}

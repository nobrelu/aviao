package br.com.itau.bootcamp.consumeropenbanking.comunication.http.controllers;

import br.com.itau.bootcamp.consumeropenbanking.comunication.http.controllers.dto.CountDTO;
import br.com.itau.bootcamp.consumeropenbanking.port.CoreUseCasePort;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/status")
@Slf4j
public class StatusController {

    private final CoreUseCasePort port;

    @GetMapping(value = "/count-status", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> buscarQuantidadeStatus() {
        return ResponseEntity.ok(port.buscaStatus());
    }



}

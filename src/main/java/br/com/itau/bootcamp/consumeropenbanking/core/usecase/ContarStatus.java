package br.com.itau.bootcamp.consumeropenbanking.core.usecase;

import br.com.itau.bootcamp.consumeropenbanking.core.domain.Status;
import br.com.itau.bootcamp.consumeropenbanking.port.HttpClientPort;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class ContarStatus {
    private final HttpClientPort port;

    public Integer execute(){
        List<Status> status = port.getStatus();

        // Faz alguma coisa by jovilno

        return status.size();
    }
}

package br.com.itau.bootcamp.consumeropenbanking.core.domain;


import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Localidade {
    private EnderecoPostal enderecoPostal;
    private List<Telefone> telefones;
    private Disponibilidade disponibilidade;
}

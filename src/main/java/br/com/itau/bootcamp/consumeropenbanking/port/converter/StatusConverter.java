package br.com.itau.bootcamp.consumeropenbanking.port.converter;


import br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.dto.status.StatusDTO;
import br.com.itau.bootcamp.consumeropenbanking.core.domain.Status;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class StatusConverter {

    public static List<Status> toDomain(List<StatusDTO> listDTO) {
        return listDTO.stream().map(
                item -> Status.builder()
                    .code(item.getCode())
                    .explanation(item.getExplanation())
                    .detectionTime(item.getDetectionTime())
                    .expectedResolutionTime(item.getExpectedResolutionTime())
                    .updateTime(item.getUpdateTime())
                    .unavailableEndpoints(item.getUnavailableEndpoints())
                    .build()
        ).collect(Collectors.toList());

    }

}

package br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class MetaDTO {
    @JsonProperty("totalRecords")
    private Integer totalRecords;

    @JsonProperty("totalPages")
    private Integer totalPages;

}

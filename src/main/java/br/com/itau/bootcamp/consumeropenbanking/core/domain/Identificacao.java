package br.com.itau.bootcamp.consumeropenbanking.core.domain;


import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Identificacao {
    private String nomeCorporativo;
    private String numeroCnpj;

}

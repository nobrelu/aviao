package br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.dto.status;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class StatusDTO {
    @JsonProperty("code")
    private String code;

    @JsonProperty("explanation")
    private String explanation;

    @JsonProperty("detectionTime")
    private String detectionTime;

    @JsonProperty("expectedResolutionTime")
    private String expectedResolutionTime;

    @JsonProperty("updateTime")
    private String updateTime;

    @JsonProperty("unavailableEndpoints")
    private List<String> unavailableEndpoints;
}

package br.com.itau.bootcamp.consumeropenbanking.core.domain;

import java.util.List;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Empresa {
    private String nome;
    private String numeroCnpj;
    private List<Contratante> contratantes;

}

package br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.dto.status;

import br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.dto.LinksDTO;
import br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.dto.MetaDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseStatusDisponibilidadeDTO {
    @JsonProperty("data")
    private DadosStatusDTO dados;

    @JsonProperty("links")
    private LinksDTO linksDTO;

    @JsonProperty("meta")
    private MetaDTO metaDTO;
}

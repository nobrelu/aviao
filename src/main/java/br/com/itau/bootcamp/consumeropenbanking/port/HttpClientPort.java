package br.com.itau.bootcamp.consumeropenbanking.port;

import br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.dto.ResponseCorrespondenteBancarioDTO;
import br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.dto.status.StatusDTO;
import br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.impl.OpenBankingImpl;
import br.com.itau.bootcamp.consumeropenbanking.core.domain.Marca;
import br.com.itau.bootcamp.consumeropenbanking.core.domain.Status;
import br.com.itau.bootcamp.consumeropenbanking.port.converter.MarcaConverter;
import br.com.itau.bootcamp.consumeropenbanking.port.converter.StatusConverter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class HttpClientPort {
    private final OpenBankingImpl comunicationOpenBanking;

    public Marca buscaCorrespondentesBancarios() {
        log.info("Inicio do PORT de busca de correspondente bancário");
        ResponseCorrespondenteBancarioDTO responseCorrespondenteBancarioDTO = comunicationOpenBanking.buscarCorrespondentesBancarios();

        Marca marca = MarcaConverter.toDomain(responseCorrespondenteBancarioDTO);

        log.info("Correspondente Bancário convertido: {} ", marca);
        return marca;
    }

    public List<Status> getStatus() {
        List<StatusDTO> status = comunicationOpenBanking.getStatus();
        return StatusConverter.toDomain(status);
     }
}

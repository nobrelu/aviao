package br.com.itau.bootcamp.consumeropenbanking.core.usecase;

import br.com.itau.bootcamp.consumeropenbanking.core.domain.*;
import br.com.itau.bootcamp.consumeropenbanking.port.HttpClientPort;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
@AllArgsConstructor
@Slf4j
public class BuscarQuantidadeCorrespondentesBancariosUC {

    private final HttpClientPort port;

    public List<List<Localidade>> execute (){
        Marca marca = port.buscaCorrespondentesBancarios();

        //int quantidadecidades = 0;

        List<List<Localidade>> lista = new ArrayList<>();


        for(Empresa empresa : marca.getEmpresas()){
            for(Contratante contratante : empresa.getContratantes()) {
                for(CorrespondenteBancario correspondenteBancario : contratante.getCorrespondentesBancarios()) {
                    if( correspondenteBancario.getLocalidades().get(0).getEnderecoPostal().getNomeCidade().equals("MONTES CLAROS") && correspondenteBancario.getLocalidades().get(0).getEnderecoPostal().getNomeBairro().equals("CENTRO")) {
                        List<Localidade> x = correspondenteBancario.getLocalidades();
                        lista.add(x);


                        //lista.add(correspondenteBancario);



                    }
//correspondenteBancario.getLocalidades().get(0).getEnderecoPostal().getNomeBairro().equals("CENTRO") &&


                }


                //quantidadeCorrespondentes += contratante.getCorrespondentesBancarios().size();
            }
        }


        return lista;
    }


}

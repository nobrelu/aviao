package br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.feign;

import br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.dto.status.ResponseStatusDisponibilidadeDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Component // o que faz?
//na url era url.openbanking // http://localhost:8080
@FeignClient(url = "${url.openbanking}", name = "cliente")
public interface OpenbankingFeign {
    //ele usou o banking-agents
    @GetMapping(value = "/channels/v1/banking-agents", produces = "application/json")
    public String buscarCorrespondentesBancarios();
    //criaram algo aqui
    @GetMapping(value = "discovery/v1/status", produces = "application/json")
    public ResponseEntity<ResponseStatusDisponibilidadeDTO> getStatus(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "page-size", required = false) Integer pageSize
    );
}

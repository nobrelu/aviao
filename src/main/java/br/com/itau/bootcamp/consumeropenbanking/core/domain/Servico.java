package br.com.itau.bootcamp.consumeropenbanking.core.domain;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Servico {
    private String nome;
    private String codigo;
}

package br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.dto;

//esse import aqui nao tem no jovilno
//import br.com.itau.bootcamp.consumeropenbanking.core.domain.*;


import com.fasterxml.jackson.annotation.JsonProperty;
//import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;


@Data // gera metodos basicos de acesso aos dados como getters e setters
@AllArgsConstructor //gera um construtor com todos os argumentos
@NoArgsConstructor //gera o construtor padrão
@ToString // sobrescreve o metodo toString padrão, para exibir os valores dos campos
@JsonIgnoreProperties(ignoreUnknown = true) // anotação que permite ignorr, caso a gente receba um campo que não mapeamos
public class ResponseCorrespondenteBancarioDTO {

    @JsonProperty(value = "data")
    private Dados dados;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @ToString
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Dados {
        @JsonProperty(value = "brand")
        private Marca marca;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @ToString
        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Marca {
            @JsonProperty(value = "name")
            private String nome;
            @JsonProperty(value = "companies")
            private List<Empresas> empresas;

            @Data
            @AllArgsConstructor
            @NoArgsConstructor
            @ToString
            @JsonIgnoreProperties(ignoreUnknown = true)
            public static class Empresas {
                @JsonProperty(value = "name")
                private String nome;
                @JsonProperty(value = "cnpjNumber")
                private String numeroCnpj;
                @JsonProperty(value = "contractors")
                private List<Contratante> contratantes;

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @ToString
                @JsonIgnoreProperties(ignoreUnknown = true)
                public static class Contratante {
                    @JsonProperty(value = "name")
                    private String nome;
                    @JsonProperty(value = "cnpjNumber")
                    private String numeroCnpj;
                    @JsonProperty(value = "bankingAgents")
                    private List<CorrespondenteBancario> correspondentesBancarios;

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @ToString
                    @JsonIgnoreProperties(ignoreUnknown = true)
                    public static class CorrespondenteBancario {
                        @JsonProperty(value = "identification")
                        private Identificacao identificacao;
                        @JsonProperty(value = "locations")
                        private List<Localidade> localidades;
                        @JsonProperty(value = "services")
                        private List<Servico> servicos;

                        @Data
                        @AllArgsConstructor
                        @NoArgsConstructor
                        @ToString
                        @JsonIgnoreProperties(ignoreUnknown = true)
                        public static class Identificacao {
                            @JsonProperty(value = "corporativoName")
                            private String nomeCorporativo;
                            @JsonProperty(value = "cnpjNumber")
                            private String numeroCnpj;
                        }
                        @Data
                        @AllArgsConstructor
                        @NoArgsConstructor
                        @ToString
                        @JsonIgnoreProperties(ignoreUnknown = true)
                        public static class Localidade {
                            @JsonProperty(value = "postalAddress")
                            private EnderecoPostal enderecoPostal;
                            @JsonProperty(value = "phones")
                            private List<Telefone> telefones;
                            @JsonProperty(value = "availability")
                            private Disponibilidade disponibilidade;

                            @Data
                            @AllArgsConstructor
                            @NoArgsConstructor
                            @ToString
                            @JsonIgnoreProperties(ignoreUnknown = true)
                            public static class EnderecoPostal {
                                @JsonProperty(value = "address")
                                private String endereco;
                                @JsonProperty(value = "districtName")
                                private String nomeBairro;
                                @JsonProperty(value = "townName")
                                private String nomeCidade;
                                @JsonProperty(value = "countrySubDivision")
                                private String siglaEstado;
                                @JsonProperty(value = "postCode")
                                private String codigoPostal;
                                @JsonProperty(value = "additionalInfo")
                                private String informacaoAdicional;

                                }
                                @Data
                                @ToString
                                @JsonIgnoreProperties(ignoreUnknown = true)
                                public static class Telefone {



                                }
                                @Data
                                @AllArgsConstructor
                                @NoArgsConstructor
                                @ToString
                                @JsonIgnoreProperties(ignoreUnknown = true)
                                public static class Disponibilidade {
                                    @JsonProperty(value = "standards")
                                    private List<Padrao> padroes;

                                    @Data
                                    @ToString
                                    @JsonIgnoreProperties(ignoreUnknown = true)
                                    public static class Padrao {

                                    }
                                }
                            }
                            @Data
                            @AllArgsConstructor
                            @NoArgsConstructor
                            @ToString
                            @JsonIgnoreProperties(ignoreUnknown = true)
                            public static class Servico {
                                @JsonProperty(value = "name")
                                private String nome;
                                @JsonProperty(value = "code")
                                private String codigo;

                        }
                    }
                }
            }
        }
    }
}

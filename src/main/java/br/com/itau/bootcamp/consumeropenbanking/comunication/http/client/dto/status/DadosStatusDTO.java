package br.com.itau.bootcamp.consumeropenbanking.comunication.http.client.dto.status;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class DadosStatusDTO {

    @JsonProperty("status")
    private List<StatusDTO> statusDTO;

}
